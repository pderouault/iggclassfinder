package fr.chulimoges.IgGClassFinder;

import org.apache.commons.cli.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Define and parse the options of the program
 */
public class CommandParser {

    private final CommandLine commandLine;
    private final Options options = new Options();

    // input fasta file
    private File fastaFile;

    // output fasta file
    private File outFile;

    public CommandParser(String[] argsList) throws ParseException {
        initOptions();
        CommandLineParser parser = new DefaultParser();
        commandLine = parser.parse(options, argsList, true);
        checkArgs();
    }

    /**
     * Initalize the options
     */
    private void initOptions() {
        final Option fastaFileOpt = Option.builder("f")
                .longOpt("fastaFile") //
                .desc("Path to the fasta file - Required")
                .hasArg(true)
                .argName("file")
                .required(true)
                .build();
        final Option outFileOpt = Option.builder("o")
                .longOpt("outfile") //
                .desc("Path to the outfile - Required")
                .hasArg(true)
                .argName("file")
                .required(true)
                .build();
        options.addOption(fastaFileOpt);
        options.addOption(outFileOpt);
    }


    /**
     * Check the validity of the parameters
     * @throws ParseException
     */
    public void checkArgs() throws ParseException {

        fastaFile = new File(commandLine.getOptionValue("fastaFile"));
        if (!fastaFile.exists() || ! fastaFile.isFile()) {
            throw new ParseException("The fasta input file is not found");
        }
        outFile = new File(commandLine.getOptionValue("outfile"));
    }

    public CommandLine getCommandLine() { return commandLine; }

    public File getFastaFile() { return fastaFile; }

    public File getOutFile() { return outFile; }
}
