package fr.chulimoges.IgGClassFinder;


public class Main {

    public static void main(String[] args) {
        try {
            CommandParser commandParser = new CommandParser(args);
            IgGParser igGParser = new IgGParser(commandParser.getFastaFile(), commandParser.getOutFile());
            igGParser.parseIgG();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
