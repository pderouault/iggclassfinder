package fr.chulimoges.IgGClassFinder;

import org.biojava.nbio.alignment.Alignments;
import org.biojava.nbio.alignment.SimpleGapPenalty;
import org.biojava.nbio.core.alignment.matrices.SubstitutionMatrixHelper;
import org.biojava.nbio.core.alignment.template.SequencePair;
import org.biojava.nbio.core.alignment.template.SubstitutionMatrix;
import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.compound.AmbiguityDNACompoundSet;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.io.FastaReaderHelper;
import org.biojava.nbio.core.sequence.io.FastaWriterHelper;
import org.biojava.nbio.core.sequence.template.Sequence;

import java.io.*;
import java.util.*;

public class IgGParser {

    private final File fastaFile;
    private File outFile;

    // reference sequence
    private final static String ref_sequence = "cctccaccaagggcccatcggtcttccccctggcaccctcctccaagagcacctctgggggcacagcggccctgggcTGCCTGGTCAAGGACTACTTC";

    // DNASequence instace of the reference sequence
    private final DNASequence reference;

    // Position of snp in the reference to dinstinguish IgG subclasses
    private final static int[] snp_index = new int[] {19,34,45,57,59};

    // Map of the 5 snps sequence to ther corresponding subclasse
    private final static Map<String, String> isotypeTagMap = new HashMap<>();

    // alignment parameters for BioJava
    private final SubstitutionMatrix<NucleotideCompound> matrix = SubstitutionMatrixHelper.getNuc4_4();
    private final SimpleGapPenalty gapP = new SimpleGapPenalty();

    public IgGParser(File fastaFile, File outFile) throws CompoundNotFoundException {
        this.fastaFile = fastaFile;
        this.outFile = outFile;

        // init alignment parameters for BioJava
        gapP.setOpenPenalty((short) 5);
        gapP.setExtensionPenalty((short) 2);

        // init snp sequences/ sublcasse map
        isotypeTagMap.put("GAAGG", "IgG1");
        isotypeTagMap.put("GGGAA", "IgG2");
        isotypeTagMap.put("GGGGG", "IgG3");
        isotypeTagMap.put("CGGAA", "IgG4");

        // Create ReferenceSequence
        reference = new DNASequence(ref_sequence, AmbiguityDNACompoundSet.getDNACompoundSet());

    }


    /**
     * Read the fasta file and align the sequence of each sequence
     * to the reference to get the base at each snp positions
     * @throws Exception
     */
    public void parseIgG() throws Exception {
        FileOutputStream outputStream = new FileOutputStream(outFile);
        BufferedOutputStream bo = new BufferedOutputStream(outputStream);
        LinkedHashMap<String, DNASequence> a = FastaReaderHelper.readFastaDNASequence(fastaFile);
        for (Map.Entry<String, DNASequence> entry : a.entrySet() ) {
            DNASequence read = entry.getValue();
            // for each IgG sequences, find this subclass
            if (getIsotype(read.getOriginalHeader()).equals("IgG")) {

                // Get the subclasse
                String igGClass = getIgGClass(entry.getValue().getSequenceAsString());

                // Update the header of the fasta with the subclasse
                read.setOriginalHeader(read.getOriginalHeader() + ";Subclasse:" + igGClass);
            }

            // Finally write the fasta sequence to the ouput file
            FastaWriterHelper.writeNucleotideSequence(bo, singleSeqToCollection(read));
        }
    }


    /**
     * @param seq Ig nucleotid sequence
     * @return IgG Subclasse name of the Ig, "Unknow" if no subclasse was found
     * @throws CompoundNotFoundException
     */
    private String getIgGClass(String seq) throws CompoundNotFoundException {
        DNASequence target = new DNASequence(seq, AmbiguityDNACompoundSet.getDNACompoundSet());
        SequencePair<DNASequence, NucleotideCompound> psa = Alignments.getPairwiseAlignment(target, reference, Alignments.PairwiseSequenceAlignerType.LOCAL, gapP, matrix);
        String tag = getIgGTag(psa);
        return isotypeTagMap.getOrDefault(tag, "Unknow");
    }

    /**
     * @param psa alignment of a IgG sequence and the reference
     * @return a sequence from the nucleotids at each SNP position
     */
    private String getIgGTag(SequencePair<DNASequence, NucleotideCompound> psa) {
        StringBuilder tag = new StringBuilder();
        for (int i : snp_index) {
            try {
                tag.append(psa.getQuery().getOriginalSequence().toString().charAt(psa.getIndexInQueryForTargetAt(i)));
            } catch (StringIndexOutOfBoundsException e) {
                // getIndexInQueryForTargetAt() return last alignment position even if > query length
                tag.append("N");
            }
        }

        return tag.toString();
    }

    /**
     * Get the isotype from the fasta header
     * Isotype must be written like this : >name;Isotype:X
     * @param header
     * @return The isotype string if presents if fasta header, empty string if not
     */
    private String getIsotype(String header) {
        for (String tags : header.split(";")) {
            String[] tokens = tags.split(":");
            if (tokens.length == 2 && tokens[0].equals("Isotype")) {
                return tokens[1];
            }
        }
        return "";
    }


    /**
     * Put a single DNASequence in a List
     * @param sequence
     * @return
     */
    private static Collection<DNASequence> singleSeqToCollection(DNASequence sequence) {
        Collection<DNASequence> sequences = new ArrayList<>();
        sequences.add(sequence);
        return sequences;
    }
}
